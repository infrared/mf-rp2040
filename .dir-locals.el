;; SPDX-FileCopyrightText: 2022 Christopher Howard <christopher@librehacker.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

((forth-mode . ((input-method-function
                . (lambda (b)
                    (list (upcase b)))))))
