[![REUSE status](https://api.reuse.software/badge/codeberg.org/infrared/mf-rp2040)](https://api.reuse.software/info/codeberg.org/infrared/mf-rp2040)

# Description

This project is meant to be a collection of words I've coded that are generically useful for programming in Mecrisp Stellaris for the RP2040 target.

# Copying (README file)

This README is part of the mf-rp2040 project.

SPDX-FileCopyrightText: 2022 Christopher Howard <christopher@librehacker.com>

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Prelude Dependencies

I have been using the 2.6.3 "with-tools" Mecrisp Stellaris image.

It is recommend to load these first of all, in this order (left-to-right):

MARKER.FS → DICTIONARY.FS → CONSTANT.FS

# Modules

## ARRAY.FS

Some words for nicer input of array data.

## CRC32.FS

A word for getting the CRC32 checksum of an array of bytes. See CRC32_DEMO.FS for demo code.

Dependency chain: ARRAY.FS

## GPIO_DIAG.FS

Diagnostic words for the GPIO registers.

Dependency chain: DIAG_COMMON.FS → GPIO_DIAG.FS

## I2C.FS (WIP)

Words meant to help with using I2C bus. Mainly they are just mappings of the associated registers and bits, and some demo code (I2C_DEMO.FS).

## I2C_DIAG.FS

Diagnostic words for the I2C registers.

Dependency chain: DIAG_COMMON.FS → I2C_DIAG.FS
